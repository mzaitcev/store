package com.store.core;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;

@ContextConfiguration(locations = {"classpath:testContext.xml"})
public class CoreTest extends AbstractTransactionalTestNGSpringContextTests  {
}
