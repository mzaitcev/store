package com.store.core.dao;

import com.store.core.CoreTest;
import com.store.core.domain.Sale;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

@Test
public class SaleDaoTest extends CoreTest{

	@Autowired
	SaleDao saleDao;

	@Autowired
	BillDao billDao;

	@Autowired
	DiscountDao discountDao;

	@Autowired
	ItemDao itemDao;
	@Test
	public void getSalesByBill() {
		List<Sale> saleList = saleDao.getListByBill(1L);
		Assert.assertNotNull(saleList);
		Assert.assertEquals(saleList.size(), 3);
	}

	@Test
	public void saveSalesTest() {
		Sale sale = new Sale();
		sale.setBill(billDao.getById(2L));
		sale.setDiscount(discountDao.getCurDiscount());
		sale.setItem(itemDao.getById(3L));
		sale.setNumber(5L);

		saleDao.saveSale(sale);

		List<Sale> saleList = saleDao.getListByBill(2L);
		Assert.assertNotNull(saleList);
		Assert.assertEquals(saleList.size(), 2);
	}

}
