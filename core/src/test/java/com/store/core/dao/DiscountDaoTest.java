package com.store.core.dao;

import com.store.core.CoreTest;
import com.store.core.domain.Discount;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

@Test
public class DiscountDaoTest extends CoreTest {

    @Autowired
    private DiscountDao discountDao;

    @Test
    public void findAllTest() {
        List<Discount> discounts = discountDao.getAllDiscounts();
        Assert.assertEquals(discounts.size(), 3);
        System.out.print(discounts);
    }

    @Test
    public void findCurrentDiscountTest() {
        Discount discount = discountDao.getCurDiscount();

        DateTime date = new DateTime();
        date = date.withYear(2015).withMonthOfYear(6).withDayOfMonth(20).withHourOfDay(3).withMinuteOfHour(44).withSecondOfMinute(30).withMillisOfSecond(926);

        Assert.assertNotNull(discount);
        Assert.assertEquals(discount.getId().longValue(), 2L);
        Assert.assertEquals(discount.getSize().intValue(), 7);
        Assert.assertEquals(discount.getItem().getId().longValue(), 2L);
        Assert.assertEquals(discount.getStartDate(), date);
    }

}
