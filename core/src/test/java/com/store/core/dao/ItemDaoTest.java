package com.store.core.dao;


import com.store.core.CoreTest;
import com.store.core.domain.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

@Test
public class ItemDaoTest extends CoreTest {
    @Autowired
    private ItemDao itemDao;

    @Test
    public void findAllTest(){
        List<Item> itemList =  itemDao.getAll();
        assertNotNull(itemList);
        assertEquals(itemList.size(), 4);
    }

    @Test
    public void findAllWithRemovedTest(){
        List<Item> itemList =  itemDao.findAllWithRemoved();
        assertNotNull(itemList);
        assertEquals(itemList.size(), 5);
    }

    @Test
    public void removeItemTest() {
        Item item = itemDao.getById(1L);
        List<Item> itemList =  itemDao.getAll();
        assertNotNull(itemList);
        assertEquals(itemList.size(), 4);
        itemDao.delete(item);
        List<Item> removedItemList =  itemDao.getAll();
        assertNotNull(removedItemList);
        assertEquals(removedItemList.size(), 3);
    }

    @Test
    public void findByIdItemTest() {
       Item item = itemDao.getById(1L);
        Assert.assertEquals(1, item.getId().longValue());
        Assert.assertEquals(20f, item.getPrice());
        Assert.assertEquals("cake", item.getName());
        Assert.assertEquals("Tasty Cake", item.getDescription());
        Assert.assertEquals(6, item.getTotalSales().intValue());
    }

    @Test
    public void  saveItemTest() {
        Item itemToAdd = new Item();
        itemToAdd.setPrice(20f);
        itemToAdd.setName("screwdriver");
        itemToAdd.setDescription("Phillips  screwdriver");

        Long id = itemDao.save(itemToAdd).getId();

        itemToAdd.setId(id);

        Item savedItem = itemDao.getById(id);

        Assert.assertEquals(itemToAdd, savedItem);

        List<Item> itemList =  itemDao.getAll();
        assertNotNull(itemList);
        assertEquals(itemList.size(), 5);
    }

    @Test
    public void  updateItemTest() {
        Item item = itemDao.getById(1L);
        item.setPrice(1000f);
        Long id = itemDao.save(item).getId();

        Item updatedItem = itemDao.getById(id);
        Assert.assertEquals(item, updatedItem);

        List<Item> itemList =  itemDao.getAll();
        assertNotNull(itemList);
        assertEquals(itemList.size(), 4);
    }

    @Test
    public void getRandomItemTest() {
        Item randItem1 = itemDao.getRandItem();
        Item randItem2 = itemDao.getRandItem();
        assertNotNull(randItem1);
        assertNotNull(randItem2);
        Assert.assertNotEquals(randItem1, randItem2);

    }

}
