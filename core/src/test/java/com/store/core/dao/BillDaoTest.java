package com.store.core.dao;

import com.store.core.CoreTest;
import com.store.core.domain.Bill;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

@Test
public class BillDaoTest extends CoreTest {

    @Autowired
    BillDao billDao;

    @Test
    public void getAllTest() {
        List<Bill> billList = billDao.getAll();
        Assert.assertEquals(billList.size(), 8);
    }

    @Test
    public void getBillByIdTest() {
        Bill bill = billDao.getById(1L);

        DateTime date = new DateTime();
        date = date.withYear(2015).withMonthOfYear(6).withDayOfMonth(20).withHourOfDay(2).withMinuteOfHour(39).withSecondOfMinute(26).withMillisOfSecond(622);

        Assert.assertNotNull(bill);
        Assert.assertEquals(bill.getId().longValue(), 1L);
        Assert.assertEquals(bill.getDate(), date);
    }

    @Test
    public void saveBillTest() {
        Bill bill = new Bill();
        Long id = billDao.save(bill).getId();
        Assert.assertNotNull(id);
        Bill savedBill = billDao.getById(id);
        bill.setId(id);
        Assert.assertEquals(bill, savedBill);
    }

    @Test
    public void getAllInTimeRangeTest() {
        DateTime stsrtDite = new DateTime(2015, 06,26, 03, 00, 00, 00);
        DateTime endTime = new DateTime(2015, 06,26, 03, 59, 59, 59);
        List<Bill> bills = billDao.getAllInTimeRange(stsrtDite, endTime);
        Assert.assertNotNull(bills);
        Assert.assertEquals(bills.size(), 4);
        Assert.assertTrue(bills.get(0).getDate().isBefore(endTime) && bills.get(0).getDate().isAfter(stsrtDite));
        Assert.assertTrue(bills.get(1).getDate().isBefore(endTime) && bills.get(0).getDate().isAfter(stsrtDite));
        Assert.assertTrue(bills.get(2).getDate().isBefore(endTime) && bills.get(0).getDate().isAfter(stsrtDite));
        Assert.assertTrue(bills.get(3).getDate().isBefore(endTime) && bills.get(0).getDate().isAfter(stsrtDite));
    }

    @Test
    public void getCountBillSizeTest() {
        Long count  = billDao.getListCount();
        Assert.assertEquals(count, new Long(8));
    }

    @Test
    public void getPaginationBills() {
        List<Bill> billList = billDao.getList(3, 2);
        Assert.assertEquals(billList.size(), 2);
    }
}
