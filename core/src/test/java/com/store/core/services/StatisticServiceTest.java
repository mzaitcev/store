package com.store.core.services;

import com.store.core.CoreTest;
import com.store.core.domain.StatisticStore;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import org.testng.Assert;
import org.testng.annotations.Test;

@Test
public class StatisticServiceTest extends CoreTest  {
    @Autowired
    StatisticService statisticService;

    @Test
    public void  getLastHourStatisticTest() {
        DateTime time = new DateTime(2015, 06,26, 03, 20, 30, 40);
        StatisticStore stat = statisticService.getLastHourStatistic(time);
        Assert.assertNotNull(stat);
        Assert.assertEquals(stat.getBillsCount(), new Integer(4));
        Assert.assertEquals(stat.getBillsCoast(), 792f);
        Assert.assertEquals(stat.getAverageBillCost(), 198.0f);
        Assert.assertEquals(stat.getSumDiscount(), 9.240001f);
        Assert.assertEquals(stat.getBillsCoastDisc(), 782.76f);
        Assert.assertEquals(stat.getAverageBillCostDisc(), 195.69f);
        Assert.assertEquals(stat.getDifferenceBills(), -55f);

    }

}
