package com.store.core.services;

import com.store.core.CoreTest;
import com.store.core.Utils;
import com.store.core.domain.Discount;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.util.Date;
import java.util.List;

@Test
public class DiscountServiceTest extends CoreTest {

    @Autowired
    private DiscountServices discountServices;

    @Test
    public void findAllTest() {
        List<Discount> discounts = discountServices.getAllDiscounts();
        Assert.assertEquals(discounts.size(), 3);
        System.out.print(discounts);
    }

    @Test
    public void findCurrentDiscountTest() {
        Discount discount = discountServices.getCurDiscount();

        DateTime date = new DateTime();
        date = date.withYear(2015).withMonthOfYear(6).withDayOfMonth(20).withHourOfDay(3).withMinuteOfHour(44).withSecondOfMinute(30).withMillisOfSecond(926);
        Assert.assertNotNull(discount);
        Assert.assertEquals(discount.getId().longValue(), 2L);
        Assert.assertEquals(discount.getSize().intValue(), 7);
        Assert.assertEquals(discount.getItem().getId().longValue(), 2L);
        Assert.assertEquals(discount.getStartDate(), date);
    }

    @Test()
    public void  generateNewDiscountTest() {

        DateTime date = new DateTime();
        discountServices.generationNewDiscount();
        Discount lastDiscount = discountServices.getCurDiscount();

        Assert.assertNotNull(lastDiscount);
        Assert.assertTrue(lastDiscount.getStartDate().compareTo(date) == 1 || lastDiscount.getStartDate().compareTo(date) == 0);
        Assert.assertTrue(lastDiscount.getSize() >= Utils.MINIMUM_DISCOUNT);
        Assert.assertNotNull(lastDiscount.getItem());
        Assert.assertTrue(lastDiscount.getSize() <= Utils.MAXIMUM_DISCOUNT);
    }

}
