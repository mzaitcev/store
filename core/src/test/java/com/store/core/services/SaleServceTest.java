package com.store.core.services;

import com.store.core.CoreTest;
import com.store.core.domain.Bill;
import com.store.core.domain.Sale;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

@Test
public class SaleServceTest extends CoreTest{

	@Autowired
	SaleService saleService;

	@Autowired
	BillService billService;

	Bill bill;

	@BeforeClass
	public void prepareData(){
		bill = billService.getById(1L);
	}

	@Test
	public void getSalesByBill() {
		Assert.assertNotNull(bill);
		List<Sale> saleList = saleService.getListByBill(bill);
		Assert.assertNotNull(saleList);
		Assert.assertEquals(saleList.size(), 3);
	}

	@Test
	public void saveSales() {
		saleService.newSale(3L, 3L, 3L);
		List<Sale> saleList = saleService.getListByBill(3L);
		Assert.assertNotNull(saleList);
		Assert.assertEquals(saleList.size(), 2);
	}

	}
