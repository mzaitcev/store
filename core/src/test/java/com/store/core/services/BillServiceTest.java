package com.store.core.services;

import com.store.core.CoreTest;
import com.store.core.domain.Bill;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Test
public class BillServiceTest extends CoreTest {

    @Autowired
    private BillService billService;

    @Test
    public void getAllTest() {
        List<Bill> billList = billService.getAll();
        Assert.assertEquals(billList.size(), 8);
    }

    @Test
    public void getBillByIdTest() {
        Bill bill = billService.getById(1L);

        DateTime date = new DateTime();
        date = date.withYear(2015).withMonthOfYear(6).withDayOfMonth(20).withHourOfDay(2).withMinuteOfHour(39).withSecondOfMinute(26).withMillisOfSecond(622);

        Assert.assertNotNull(bill);
        Assert.assertEquals(bill.getId().longValue(), 1L);
        Assert.assertEquals(bill.getDate(), date);
    }

    @Test
    public void saveBillTest() {
        Bill bill = new Bill();
        Bill savedBill = billService.saveBill(bill);
        Assert.assertNotNull(savedBill);
    }

}
