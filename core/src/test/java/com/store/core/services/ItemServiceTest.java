package com.store.core.services;


import com.store.core.CoreTest;
import com.store.core.domain.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

@Test
public class ItemServiceTest extends CoreTest {
    @Autowired
    private ItemService itemService;

    @Test
    public void findAllTest() {
        List<Item> itemList = itemService.findAll();
        assertNotNull(itemList);
        assertEquals(itemList.size(), 4);
    }

    @Test
    public void findAllWithRemovedTest() {
        List<Item> itemList = itemService.findAllWithRemoved();
        assertNotNull(itemList);
        assertEquals(itemList.size(), 5);
    }

    @Test
    public void removeItemTest() {
        Item item = itemService.findById(1L);
        List<Item> itemList = itemService.findAll();
        assertNotNull(itemList);
        assertEquals(itemList.size(), 4);
        itemService.delete(item);
        List<Item> removedItemList = itemService.findAll();
        assertNotNull(removedItemList);
        assertEquals(removedItemList.size(), 3);
    }

    @Test
    public void findByIdItemTest() {
        Item item = itemService.findById(1L);
        Assert.assertEquals(1, item.getId().longValue());
        Assert.assertEquals(20f, item.getPrice());
        Assert.assertEquals("cake", item.getName());
        Assert.assertEquals("Tasty Cake", item.getDescription());
    }

    @Test
    public void saveItemTest() {
        Item itemToAdd = new Item();
        itemToAdd.setPrice(20f);
        itemToAdd.setName("screwdriver");
        itemToAdd.setDescription("Phillips  screwdriver");

        Item savedItem = itemService.save(itemToAdd);
        Assert.assertNotNull(savedItem);
    }


    @Test
    public void updateItemTest() {
        Item item = itemService.findById(1L);
        item.setDescription("Update description");
        Item updatedItem = itemService.save(item);
        Assert.assertNotNull(updatedItem);
        Assert.assertEquals(item, updatedItem);
    }

}
