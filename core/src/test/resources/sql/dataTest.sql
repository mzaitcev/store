INSERT INTO public.item (price, name, description, id, deleted) VALUES (20, 'cake', 'Tasty Cake', 1, FALSE);
INSERT INTO public.item (price, name, description, id, deleted) VALUES (15, 'eclair', 'Tasty Eclair', 2, FALSE);
INSERT INTO public.item (price, name, description, id, deleted) VALUES (10, 'potato', 'Round potatoes', 3, FALSE);
INSERT INTO public.item (price, name, description, id, deleted) VALUES (5, 'carrots', 'Fresh Carrots', 4, FALSE);
INSERT INTO public.item (price, name, description, id, deleted) VALUES (50, 'tomato', 'Red tomato', 5, TRUE);

INSERT INTO public.discount (id, size, startdate, id_item) VALUES (1, 5, '2015-06-20 02:44:19.270000', 1);
INSERT INTO public.discount (id, size, startdate, id_item) VALUES (2, 7, '2015-06-20 03:44:30.926000', 2);
INSERT INTO public.discount (id, size, startdate, id_item) VALUES (3, 6, '2015-06-20 01:44:44.465000', 3);

INSERT INTO public.bill (id, date) VALUES (1, '2015-06-20 02:39:26.622000');
INSERT INTO public.bill (id, date) VALUES (2, '2015-06-20 02:43:15.266000');
INSERT INTO public.bill (id, date) VALUES (3, '2015-06-18 02:43:20.677000');
INSERT INTO public.bill (id, date) VALUES (4, '2015-06-26 02:43:32.496000');

INSERT INTO public.bill (id, date) VALUES (5, '2015-06-26 03:46:32.496000');
INSERT INTO public.bill (id, date) VALUES (6, '2015-06-26 03:47:34.496000');
INSERT INTO public.bill (id, date) VALUES (7, '2015-06-26 03:48:35.496000');
INSERT INTO public.bill (id, date) VALUES (8, '2015-06-26 03:49:36.496000');

INSERT INTO public.sale ("id_item", "id_discount", "id_bill", number, id, price) VALUES (1, 1, 1, 5, 1, 10);
INSERT INTO public.sale ("id_item", "id_discount", "id_bill", number, id, price) VALUES (3, null , 1, 10, 2, 20);
INSERT INTO public.sale ("id_item", "id_discount", "id_bill", number, id, price) VALUES (5, null, 1, 7, 3, 10);
INSERT INTO public.sale ("id_item", "id_discount", "id_bill", number, id, price) VALUES (2, null, 2, 15, 4,20);
INSERT INTO public.sale ("id_item", "id_discount", "id_bill", number, id, price) VALUES (4, null, 3, 20, 6, 11);
INSERT INTO public.sale ("id_item", "id_discount", "id_bill", number, id, price) VALUES (1, null, 4, 1, 7, 33);
INSERT INTO public.sale ("id_item", "id_discount", "id_bill", number, id, price) VALUES (2, null, 4, 1, 8, 33);
INSERT INTO public.sale ("id_item", "id_discount", "id_bill", number, id, price) VALUES (3, null, 4, 1, 9, 22);
INSERT INTO public.sale ("id_item", "id_discount", "id_bill", number, id, price) VALUES (4, null, 4, 1, 10, 22);
INSERT INTO public.sale ("id_item", "id_discount", "id_bill", number, id, price) VALUES (5, null, 4, 1, 11, 33);

INSERT INTO public.sale ("id_item", "id_discount", "id_bill", number, id, price) VALUES (5, null, 5, 2, 12,33);
INSERT INTO public.sale ("id_item", "id_discount", "id_bill", number, id, price) VALUES (2, 2, 6, 3, 13, 33);
INSERT INTO public.sale ("id_item", "id_discount", "id_bill", number, id, price) VALUES (5, null, 7, 4, 14, 33);
INSERT INTO public.sale ("id_item", "id_discount", "id_bill", number, id, price) VALUES (5, null, 8, 5, 15, 33);
INSERT INTO public.sale ("id_item", "id_discount", "id_bill", number, id, price) VALUES (2, 2, 5, 1, 16, 33);
INSERT INTO public.sale ("id_item", "id_discount", "id_bill", number, id, price) VALUES (5, null, 6, 9, 17, 33);