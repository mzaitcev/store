package com.store.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.*;
import org.jadira.usertype.dateandtime.joda.PersistentDateTime;
import org.joda.time.DateTime;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Data
@Entity
@Table(name = "bill")
@TypeDefs({
        @TypeDef(name = "dateTime", defaultForType = DateTime.class, typeClass = PersistentDateTime.class)
})
public class Bill implements Serializable {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Long id;
    private DateTime date;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "bill")
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonIgnore
    private Set<Sale> sales;

    public Bill() {
        date = new DateTime();
    }
}
