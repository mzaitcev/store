package com.store.core.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.jadira.usertype.dateandtime.joda.PersistentDateTime;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "discount")
@TypeDefs({
        @TypeDef(name = "dateTime", defaultForType = DateTime.class, typeClass = PersistentDateTime.class)
})
public class Discount implements Serializable {

    @Id
    @GeneratedValue(generator = "increment" )
    @GenericGenerator(name="increment", strategy = "increment")
    private Long id;

    private Integer size;

    @OneToOne
    @JoinColumn(name = "id_item")
    private Item item;

    private DateTime startDate;

}
