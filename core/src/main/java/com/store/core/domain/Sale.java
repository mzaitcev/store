package com.store.core.domain;


import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(exclude = {"bill"})
@ToString(exclude = {"bill"})
@Entity
@Table(name = "sale")
public class Sale implements Serializable {
    @Id
    @GeneratedValue(generator = "increment" )
    @GenericGenerator(name="increment", strategy = "increment")
    private Long id;

    @OneToOne
    @JoinColumn(name = "id_item")
    private Item item;

    @OneToOne
    @JoinColumn(name = "id_discount")
    private Discount discount;

    @OneToOne
    @JoinColumn(name = "id_bill")
    private Bill bill;

    private Float price;

    private Long number;


}
