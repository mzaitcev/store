package com.store.core.domain;


import lombok.Data;
import org.joda.time.DateTime;

@Data
public class StatisticStore {
 DateTime startDateRange;

 Integer billsCount;

 Float billsCoast;

 Float averageBillCost;

 Float sumDiscount;

 Float billsCoastDisc;

 Float averageBillCostDisc;

 Float differenceBills;

}
