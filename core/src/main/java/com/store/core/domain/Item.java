package com.store.core.domain;

import lombok.Data;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "item")
public class Item implements Serializable {


    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Long id;

    private Float price;
    private String name;
    private String description;
    private Boolean deleted = false;

    @Formula("(select sum(s.number) from Sale s where s.id_Item = id)")
    private Integer totalSales;

}
