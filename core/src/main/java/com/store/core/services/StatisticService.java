package com.store.core.services;

import com.store.core.dao.BillDao;
import com.store.core.domain.Bill;
import com.store.core.domain.Sale;
import com.store.core.domain.StatisticStore;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatisticService {
    @Autowired
    BillDao billDao;

    public StatisticStore getLastHourStatistic(DateTime time) {
        StatisticStore statisticStore = new StatisticStore();
        DateTime startTime = new DateTime(time.getYear(), time.getMonthOfYear(), time.getDayOfMonth(), time.getHourOfDay(), 0, 0);
        DateTime endTime = new DateTime(time.getYear(), time.getMonthOfYear(), time.getDayOfMonth(), time.getHourOfDay(), 59, 59);
        List<Bill> bills = billDao.getAllInTimeRange(startTime, endTime);
        List<Bill> prevBills = billDao.getAllInTimeRange(startTime.minusHours(1), endTime.minusHours(1));
        statisticStore.setBillsCount(bills.size());
        Float billsCost = 0f;
        Float prevBillsCost = 0f;
        Float sumDiscount = 0f;
        for (Bill bill : bills) {
            for (Sale sale : bill.getSales()) {
                Float itemCoast = sale.getPrice() * sale.getNumber();
                Float itemDisc = 0f;
                if (null != sale.getDiscount()) {
                    itemDisc = itemCoast / 100 * sale.getDiscount().getSize();
                }
                sumDiscount = sumDiscount + itemDisc;
                billsCost = billsCost + itemCoast;
            }
        }

        for (Bill bill: prevBills) {
            for (Sale sale : bill.getSales()) {
                Float itemCoast = sale.getPrice() * sale.getNumber();
                prevBillsCost = prevBillsCost + itemCoast;
            }
        }

        statisticStore.setBillsCoast(billsCost);
        statisticStore.setAverageBillCost(billsCost / statisticStore.getBillsCount());
        statisticStore.setSumDiscount(sumDiscount);
        statisticStore.setBillsCoastDisc(billsCost - sumDiscount);

        statisticStore.setAverageBillCostDisc(statisticStore.getBillsCoastDisc()/statisticStore.getBillsCount());

        statisticStore.setDifferenceBills(prevBillsCost/prevBills.size() - statisticStore.getAverageBillCost());
        statisticStore.setStartDateRange(startTime);
        return statisticStore;
    }

    List<StatisticStore> getLastHourStatisticFromNow(int hourCount) {
        return null;
    }
}
