package com.store.core.services;

import com.store.core.dao.BillDao;
import com.store.core.domain.Bill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BillService {

    @Autowired
    private BillDao billDao;

    public List<Bill> getAll(){
        return billDao.getAll();
    }

    public Bill getById(Long id){
        return billDao.getById(id);
    }

    public  Bill saveBill(Bill bill){
        Long id = billDao.save(bill).getId();
        Bill savedBill = null;
        if (id != null) {
            savedBill = billDao.getById(id);
        }
        return savedBill;
    }
}
