package com.store.core.services;

import com.store.core.dao.ItemDao;
import com.store.core.domain.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemService {

    @Autowired
    private ItemDao itemDao;

    public List<Item> findAll() {
        return itemDao.getAll();
    }

    public List<Item> findAllWithRemoved() {
        return itemDao.findAllWithRemoved();
    }

    public Item findById(Long id) {
        return itemDao.getById(id);
    }

    public Item save(Item item) {
        Long id = itemDao.save(item).getId();
        Item savedItem = null;
        if (id != null) {
            savedItem = itemDao.getById(id);
        }
        return savedItem;
    }

    public void delete(Item item) {
        itemDao.delete(item);
    }
}
