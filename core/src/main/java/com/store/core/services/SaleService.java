package com.store.core.services;

import com.store.core.dao.SaleDao;
import com.store.core.domain.Bill;
import com.store.core.domain.Discount;
import com.store.core.domain.Sale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SaleService {

    @Autowired
    private SaleDao saleDao;

    @Autowired
    private ItemService itemService;

    @Autowired
    private DiscountServices discountServices;

    @Autowired
    private BillService billService;

    public List<Sale> getListByBill(Bill bill) {
        return getListByBill(bill.getId());
    }

    public List<Sale> getListByBill(Long billId) {
        return saleDao.getListByBill(billId);
    }

    public void newSale(Long billId, Long itemId, Long itemNumber) {
        Sale sale = new Sale();
        sale.setBill(billService.getById(billId));

        Discount discount = discountServices.getCurDiscount();
        if (discount.getItem().getId().equals(itemId))
            sale.setDiscount(discount);

        sale.setItem(itemService.findById(itemId));
        sale.setPrice(sale.getItem().getPrice());
        sale.setNumber(itemNumber);
        saleDao.saveSale(sale);
    }

}
