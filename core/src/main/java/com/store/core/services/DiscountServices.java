package com.store.core.services;

import com.store.core.Utils;
import com.store.core.dao.DiscountDao;
import com.store.core.dao.ItemDao;
import com.store.core.domain.Discount;
import com.store.core.domain.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class DiscountServices {

    @Autowired
    private DiscountDao discountDao;

    @Autowired
    private ItemDao itemDao;


    public Discount getCurDiscount(){
        return discountDao.getCurDiscount();
    }

    public List<Discount> getAllDiscounts(){
        return discountDao.getAllDiscounts();
    }

    @Scheduled(fixedDelay = 60*60*1000) //min*sec*milesec
    public void generationNewDiscount(){
        Random rand = new Random();
        int randomDiscoutSize = rand.nextInt((Utils.MAXIMUM_DISCOUNT - Utils.MINIMUM_DISCOUNT) + 1) + Utils.MINIMUM_DISCOUNT;
        Item item = itemDao.getRandItem();
        discountDao.generateNewDiscount(item, randomDiscoutSize);
    }
}
