package com.store.core.dao.daoImplement;

import com.store.core.Utils;
import com.store.core.dao.DiscountDao;
import com.store.core.domain.Discount;
import com.store.core.domain.Item;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Random;

@Repository("DiscountDao")
public class DiscountDaoImpl extends DaoImpl<Discount> implements DiscountDao {

    @Transactional(readOnly = true)
    public Discount getCurDiscount() {
        Query query = getSession().createQuery("Select d from Discount d where d.startDate = (select max(startDate) from Discount)");
        List<Discount> discounts = query.list();
        return discounts.get(0);
    }

    @Transactional(readOnly = true)
    public List<Discount> getAllDiscounts() {
        Criteria criteria = getSession().createCriteria(Discount.class);
        List<Discount> discounts = (List<Discount>) criteria.list();
        return discounts;
    }

    @Transactional
    public void generateNewDiscount(Item item, int discountSize) {
        Discount discount = new Discount();
        discount.setStartDate(new DateTime());
        discount.setSize(discountSize);
        discount.setItem(item);
        getSession().save(discount);
    }

}
