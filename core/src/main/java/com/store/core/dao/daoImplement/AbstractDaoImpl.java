package com.store.core.dao.daoImplement;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class AbstractDaoImpl {

    @Autowired
    SessionFactory sessionFactory;

    Session getSession(){
        return sessionFactory.getCurrentSession();
    }

}
