package com.store.core.dao;

import com.store.core.domain.Sale;

import java.util.List;

public interface SaleDao extends Dao<Sale> {

	List<Sale> getListByBill(Long Id);

	Long saveSale(Sale sale);
}
