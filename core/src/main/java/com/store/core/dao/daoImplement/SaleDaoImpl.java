package com.store.core.dao.daoImplement;

import com.store.core.dao.SaleDao;
import com.store.core.domain.Sale;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("SaleDao")
public class SaleDaoImpl extends DaoImpl<Sale> implements SaleDao{

	@Transactional
	public List<Sale> getListByBill(Long id) {
		Query query = getSession().createQuery("from Sale s where s.bill.id = :id");
		query.setParameter("id", id);
		List<Sale> sales = query.list();
		return sales;
	}

	@Transactional
	public Long saveSale(Sale sale) {
		Long id = (Long) getSession().save(sale);
		return id;
	}
}
