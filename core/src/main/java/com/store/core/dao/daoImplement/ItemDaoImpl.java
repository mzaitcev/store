package com.store.core.dao.daoImplement;

import com.store.core.dao.ItemDao;
import com.store.core.domain.Bill;
import com.store.core.domain.Item;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository("ItemDao")
public class ItemDaoImpl extends DaoExtendedImpl<Item> implements ItemDao {

    @Override
    protected Class getObjectClass() {
        return Item.class;
    }

	@Override
    public List<Item> getAll() {
        Query query = getSession().createQuery("from Item i where i.deleted = false");
        List<Item> items = query.list();
        return items;
    }

    @Transactional
	public List<Item> findAllWithRemoved() {
		Criteria criteria = getSession().createCriteria(Item.class);
		List<Item> items = (List<Item>) criteria.list();
		return items;
	}


	@Transactional
	public void delete(Item item) {
		item.setDeleted(true);
		getSession().update(item);
	}

    @Transactional
    public Item getRandItem() {
		Query query = getSession().createQuery("from Item i where i.deleted = false  order by rand()").setMaxResults(1);
		List<Item> items = query.list();
		return items.get(0);
    }

    @Override
    @Deprecated
    public void delete(Long itemId){
        return;
    }
}
