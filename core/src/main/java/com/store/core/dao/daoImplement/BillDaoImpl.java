package com.store.core.dao.daoImplement;

import com.store.core.dao.BillDao;
import com.store.core.domain.Bill;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("BillDao")
public class BillDaoImpl extends DaoExtendedImpl<Bill> implements BillDao {

    @Override
    protected Class getObjectClass() {
        return Bill.class;
    }



    @Transactional
    public List<Bill> getAllInTimeRange(DateTime startDate, DateTime endTime) {
        Criteria criteria = getSession().createCriteria(Bill.class);
        criteria.add(Restrictions.ge("date", startDate));
        criteria.add(Restrictions.lt("date", endTime));
        List<Bill> bills = (List<Bill>) criteria.list();
        return bills;
    }

}
