package com.store.core.dao.daoImplement;

import com.store.core.dao.CrudDao;
import org.hibernate.Criteria;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

public abstract class CrudDaoImpl <E extends Serializable> extends DaoImpl<E> implements CrudDao<E> {

    protected abstract Class getObjectClass();

    @Override
    public List<E> getAll() {
        return getSession().createCriteria(getObjectClass())
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
    }

    @Override
    public E save(E entity) {
        getSession().persist(entity);
        return entity;
    }

    @Override
    public void update(E entity) {
        getSession().update(entity);
    }

    @Override
    public void delete(Long id) {
        E entity = (E) getSession().get(getObjectClass(), id);
        getSession().delete(entity);
    }

    @Transactional
    public E getById(Long id) {
        E entity = (E) getSession().get(getObjectClass(), id);
        return entity;
    }
}
