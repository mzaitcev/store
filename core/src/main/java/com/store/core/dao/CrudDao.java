package com.store.core.dao;

import java.io.Serializable;
import java.util.List;

public interface CrudDao <E extends Serializable> extends Dao<E> {

    E getById(Long id);

    List<E> getAll();

    E save(E entity);

    void update(E entity);

    void delete(Long id);

}
