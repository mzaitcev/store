package com.store.core.dao;

import com.store.core.domain.Item;

import java.util.List;

public interface ItemDao extends DaoExtended<Item> {

    List<Item> findAllWithRemoved();

    Item getRandItem();

    void delete(Item item);

}
