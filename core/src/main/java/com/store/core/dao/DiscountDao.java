package com.store.core.dao;

import com.store.core.domain.Discount;
import com.store.core.domain.Item;

import java.util.List;

public interface DiscountDao extends Dao<Discount>{

	Discount getCurDiscount();

	List<Discount> getAllDiscounts();

	void generateNewDiscount(Item item, int discountSize);

}
