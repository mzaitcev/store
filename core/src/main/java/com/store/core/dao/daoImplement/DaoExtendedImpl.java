package com.store.core.dao.daoImplement;

import com.store.core.dao.DaoExtended;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;

import java.io.Serializable;
import java.util.List;

public abstract class DaoExtendedImpl  <E extends Serializable> extends CrudDaoImpl<E> implements DaoExtended<E> {

    public List<E> getList(int from, int size) {
        Criteria criteria = getSession().createCriteria(getObjectClass());
        criteria = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        if (size >= 0) {
            criteria = criteria.setMaxResults(size);
            criteria = criteria.setFirstResult(from);
        }
        return criteria.list();
    }

    public Long getListCount() {
        Criteria criteria = getSession().createCriteria(getObjectClass());
        criteria = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria = criteria.setProjection(Projections.rowCount());
        return (Long) criteria.uniqueResult();
    }

}
