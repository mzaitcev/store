package com.store.core.dao;

import java.io.Serializable;
import java.util.List;

public interface DaoExtended <E extends Serializable> extends CrudDao<E> {

    List<E> getList(int from, int size);

    Long getListCount();

}
