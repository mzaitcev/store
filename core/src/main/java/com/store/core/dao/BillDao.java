package com.store.core.dao;

import com.store.core.domain.Bill;
import org.joda.time.DateTime;

import java.util.List;

public interface BillDao extends DaoExtended<Bill> {

    List<Bill> getAllInTimeRange(DateTime startDate, DateTime endTime);

}
