
drop TABLE if EXISTS "sale";
drop TABLE if EXISTS "bill";
drop TABLE if EXISTS "discount";
drop TABLE if EXISTS "item";


SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

CREATE  TABLE IF NOT EXISTS bill (
    id integer NOT NULL,
    date timestamp with time zone
);


ALTER TABLE bill OWNER TO postgres;

CREATE SEQUENCE bill_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


ALTER TABLE bill_id_seq OWNER TO postgres;

ALTER SEQUENCE bill_id_seq OWNED BY bill.id;

CREATE TABLE IF NOT EXISTS discount (
    id integer NOT NULL,
    size integer,
    id_item integer,
    startdate timestamp with time zone
);


ALTER TABLE discount OWNER TO postgres;

CREATE SEQUENCE discount_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


ALTER TABLE discount_id_seq OWNER TO postgres;

ALTER SEQUENCE discount_id_seq OWNED BY discount.id;

CREATE TABLE IF NOT EXISTS item (
    price bigint,
    name name,
    description text,
    deleted boolean,
    id integer NOT NULL
);

ALTER TABLE item OWNER TO postgres;

CREATE SEQUENCE item_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

ALTER TABLE item_id_seq OWNER TO postgres;

ALTER SEQUENCE item_id_seq OWNED BY item.id;

CREATE TABLE sale (
    "id_item" integer,
    "id_discount" integer,
    "id_bill" integer,
    number integer,
    price bigint,
    id integer NOT NULL
);

ALTER TABLE sale OWNER TO postgres;

CREATE SEQUENCE sale_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

ALTER TABLE sale_id_seq OWNER TO postgres;

ALTER SEQUENCE sale_id_seq OWNED BY sale.id;

ALTER TABLE ONLY bill ALTER COLUMN id SET DEFAULT nextval('bill_id_seq'::regclass);

ALTER TABLE ONLY discount ALTER COLUMN id SET DEFAULT nextval('discount_id_seq'::regclass);

ALTER TABLE ONLY item ALTER COLUMN id SET DEFAULT nextval('item_id_seq'::regclass);

ALTER TABLE ONLY sale ALTER COLUMN id SET DEFAULT nextval('sale_id_seq'::regclass);


SELECT pg_catalog.setval('bill_id_seq', 1, false);


SELECT pg_catalog.setval('discount_id_seq', 1, false);


SELECT pg_catalog.setval('item_id_seq', 1, false);


SELECT pg_catalog.setval('sale_id_seq', 1, false);

ALTER TABLE ONLY bill
ADD CONSTRAINT bill_pkey PRIMARY KEY (id);

ALTER TABLE ONLY item
ADD CONSTRAINT item_pkey PRIMARY KEY (id);


ALTER TABLE ONLY discount
ADD CONSTRAINT discount_pkey PRIMARY KEY (id);

ALTER TABLE ONLY discount
ADD CONSTRAINT "discount_idItem_fkey" FOREIGN KEY ("id_item") REFERENCES item(id);


ALTER TABLE ONLY sale
ADD CONSTRAINT sale_pkey PRIMARY KEY (id);

ALTER TABLE ONLY sale
ADD CONSTRAINT "sale_idBill_fkey" FOREIGN KEY ("id_bill") REFERENCES bill(id);

ALTER TABLE ONLY sale
ADD CONSTRAINT "sale_idDiscount_fkey" FOREIGN KEY ("id_discount") REFERENCES discount(id);

ALTER TABLE ONLY sale
ADD CONSTRAINT "sale_idItem_fkey" FOREIGN KEY ("id_item") REFERENCES item(id);



REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;