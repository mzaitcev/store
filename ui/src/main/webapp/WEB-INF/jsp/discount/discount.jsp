<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<tiles:insertDefinition name="template.main">
  <tiles:putAttribute name="content" type="string">
    <table id="discountTable">
      <tr>
        <th>Id</th>
        <th>Discount</th>
        <th>Item Name</th>
        <th>Start Date</th>
      </tr>
      <c:forEach items="${discounts}" var="discount">
        <tr>
          <td><c:out value="${discount.id}"/></td>
          <td><c:out value="${discount.size}%"/></td>
          <td><c:out value="${discount.item.name}"/></td>
          <td><joda:format value="${discount.startDate}" pattern="dd.MM.yyyy" /></td>
          <%--<td><fmt:formatDate value="${discount.startDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>--%>
        </tr>
      </c:forEach>
    </table>
    </div>
  </tiles:putAttribute>
</tiles:insertDefinition>