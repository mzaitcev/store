<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <%--<script type="text/javascript" src="<c:url value="/resources/scripts/jquery-1.11.3.min.js"/>"></script>--%>
    <link rel="stylesheet" href="<c:url value="/resources/ui/jquery-ui.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/resources/main.css"/>">
    <script src="<c:url value="/resources/ui/external/jquery/jquery.js"/>"></script>
    <script src="<c:url value="/resources/ui/jquery-ui.min.js"/>"></script>

    <title></title>
    <tiles:insertAttribute name="head"/>
</head>
<body>
<div class ="content" >
    <nav id="main">
        <a href="<c:url value="/items"/>">Items</a>
        <a href="<c:url value="/sales"/>">Sales</a>
        <a href="<c:url value="/discount"/>">Discount</a>
        <a href="<c:url value="/statistic"/>">Statistic</a>
    </nav>


    <div id="siteBodyContent">
        <tiles:insertAttribute name="content"/>
    </div>
</div>

</body>

<script type="text/javascript">


</script>
</html>
