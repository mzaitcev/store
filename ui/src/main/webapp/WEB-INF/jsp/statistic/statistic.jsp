<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="template.main">
    <tiles:putAttribute name="content" type="string">
        <div>
        </div>
        <table id="statisticContent">
            <tr>
                <th>Hour Statistic</th>
            </tr>
            <tr>
                <td colspan="2">
                    <p>Date: <input type="text" id="datepicker">
                        <select name="time" id="timePicker">
                            <option>00</option>
                            <option>01</option>
                            <option selected="selected">02</option>
                            <option>03</option>
                            <option>04</option>
                            <option>05</option>
                            <option>06</option>
                            <option>07</option>
                            <option>08</option>
                            <option>09</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                            <option>13</option>
                            <option>14</option>
                            <option>15</option>
                            <option>16</option>
                            <option>17</option>
                            <option>18</option>
                            <option>19</option>
                            <option>20</option>
                            <option>21</option>
                            <option>22</option>
                            <option>23</option>
                        </select>
                    </p>
                    <input id="getStatistic" type="button" class="ui-button ui-widget ui-corner-all" value="Show" />
                </td>
            </tr>
            <tr class="statistic">
                <td>
                    Bills Count
                </td>
                <td id="billsCount">
                </td>
            </tr>
            <tr class="statistic">
                <td>
                    Bills cost
                </td>
                <td id="billsCost">
                </td>
            </tr>
            <tr class="statistic">
                <td>
                    Average Bill Cost
                </td>
                <td id="averageBillCost">
                </td>
            </tr>
            <tr class="statistic">
                <td>
                    Sum Discount
                </td>
                <td id="sumDiscount">
                </td>
            </tr>

            <tr class="statistic">
                <td>
                    Bills Discount Cost
                </td>
                <td id="billsCostDisc">
                </td>
            </tr>

            <tr class="statistic">
                <td>
                    Average Bills Discount Cost
                </td>

                <td id="averageBillCostDisc">
                </td>
            </tr>

            <tr class="statistic">
                <td>
                    Difference Bills
                </td>
                <td id="differenceBills">
                </td>
            </tr>

        </table>

        <script type="text/javascript">
            $(document).ready(function () {
                $(function () {
                    $("#datepicker").datepicker();
                    $("#datepicker").datepicker("setDate", "06/26/2015");
                    $("#timePicker").selectmenu();


                    $("#getStatistic").click(function(){
                        var currentDate = $( "#datepicker" ).datepicker( "getDate" );
                        var dateString = $.datepicker.formatDate("dd-mm-yy", currentDate);
                        var time = $( "#timePicker").val();
                        var reqTime = dateString + time + ":00:00";
                        $.getJSON("<c:url value="/api/statistic/"/>" +reqTime, {})
                            .done(function (data) {

                                    console.log(data);
                                    debugger;
                                    $("#billsCount").text(data.billsCount);
                                    $("#billsCost").text(data.billsCoast);
                                    $("#averageBillCost").text(data.averageBillCost);
                                    $("#sumDiscount").text(data.sumDiscount);
                                    $("#billsCostDisc").text(data.billsCoastDisc);
                                    $("#averageBillCostDisc").text(data.averageBillCostDisc);
                                    $("#differenceBills").text(data.differenceBills);
                                });


                    });

                });

            });
        </script>

    </tiles:putAttribute>
</tiles:insertDefinition>