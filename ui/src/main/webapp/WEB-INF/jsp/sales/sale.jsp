<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="template.main">
    <tiles:putAttribute name="content" type="string">


        <div>
            <input id="addSale" type="button" class="ui-button ui-widget ui-corner-all" value="Add new Bill"/>
            <input id="addItem" type="button" class="ui-button ui-widget ui-corner-all" value="Add item"/>
        </div>

        <div id="dialogNewItem" title="Item">
            <div>
                idSale: <input type="number" name="name" id="idSale"/>
                idItem: <input type="number" name="name" id="idItem"/>
                number Items: <input type="number" name="name" id="itemNumber"/>
                <input id="addItemToSale" type="button" class="ui-button ui-widget ui-corner-all" value="Add item to"/>
            </div>
        </div>
        <div id="dialog" title="Sale items">
            <div>
                <table id="itemsTable">
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Number</th>
                        <th>Discount</th>
                    </tr>
                </table>
            </div>
        </div>


        <div>
            <table id="saleTable">
                <c:forEach items="${sales}" var="sale">
                    <tr>
                        <td><c:out value="${sale.id}"/></td>
                        <td><c:out value="${sale.date}"/></td>
                        <td><a class="showItemAction" data-saleid="${sale.id}" href="javascript:void(0)">Show Items</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#dialog").dialog({
                    autoOpen: false,
                    show: {
                        effect: "blind",
                        duration: 10
                    },
                    hide: {
                        effect: "blind",
                        duration: 10
                    }
                });

                $("#dialogNewItem").dialog({
                    autoOpen: false,
                    show: {
                        effect: "blind",
                        duration: 10
                    },
                    hide: {
                        effect: "blind",
                        duration: 10
                    }
                });

                $("#addItem").click(function () {
                    $("#dialogNewItem").dialog("open");
                });

                $("#addItemToSale").click(function () {
                    $.ajax({
                        method: "POST",
                        url: "<c:url value="/api/sales/newItem"/>" + "?" + "idSale=" + $("#idSale").val() + "&" + "idItem=" + $("#idItem").val() + "&" + "itemNumber=" + $("#itemNumber").val()
                    })
                        .done(function () {
                            alert("Item add");
                            window.location.reload()
                        })
                });


                $("#addSale").click(function () {
                    $.ajax({
                        method: "POST",
                        url: "<c:url value="/api/sales/newSale"/>"
                    })
                        .done(function () {
                            alert("Item add");
                            window.location.reload()
                        })
                });


                $(".showItemAction").click(function () {
                    $("#itemsTable tr").remove();
                    $.getJSON("<c:url value="/api/sales/"/>" + $(this).data("saleid"), {})
                        .done(function (data) {
                            $.each(data, function (i, item) {
                                var row = $("<tr>");
                                $("<td>").text(item.item.id).appendTo(row);
                                $("<td>").text(item.item.name).appendTo(row);
                                $("<td>").text(item.item.description).appendTo(row);
                                $("<td>").text(item.item.price).appendTo(row);
                                $("<td>").text(item.number).appendTo(row);
                                if (item.discount != null)
                                    $("<td>").text(item.discount.size).appendTo(row);
                                else
                                    $("<td>").text("none").appendTo(row);
                                $("#itemsTable tbody").append(row);
                            });
                            $("#dialog").dialog("open");
                        });
                })
            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>