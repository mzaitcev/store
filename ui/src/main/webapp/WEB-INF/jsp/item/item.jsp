<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="template.main">

    <tiles:putAttribute name="content" type="string">

        <div id="dialog" title="Item">
            <div>
                <form id="itemForm">
                    <input type="hidden" name="id" value="3" id="idItem">
                    <div>
                        Name: <input type="text" name="name" id="itemName"/>
                    </div>
                    <div>
                        Description: <input type="text" name="description" id="itemDescription"/>
                    </div>
                    <div>
                        Price: <input type="number" name="price" id="itemPrice"/>
                    </div>
                    <div>
                        <input id="clearItem" type="button" value="Clear"/>
                        <input id="saveItem" type="button" value="Save"/>
                    </div>
                </form>
            </div>
        </div>
        <div>
            <input id="newItem" type="button" class="ui-button ui-widget ui-corner-all" value="Add new item" />
            <table id="itemsTable">
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Total Sales</th>
                    <th>Actions</th>
                </tr>
                <c:forEach items="${items}" var="item">
                    <tr>
                        <td><c:out value="${item.id}"/></td>
                        <td><c:out value="${item.name}"/></td>
                        <td><c:out value="${item.description}"/></td>
                        <td><c:out value="${item.price}"/></td>
                        <td><c:out value="${item.totalSales}"/></td>
                        <td><a class="removeItemAction" data-itemId="${item.id}" href="javascript:void(0)">Remove</a>
                            <a class="editItemAction" data-itemId="${item.id}" data-itemName="${item.name}"
                               data-itemDescription="${item.description}" data-itemPrise="${item.price}"
                               href="javascript:void(0)">Edit</a></td>
                    </tr>
                </c:forEach>
            </table>
        </div>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#newItem").click(function(){
                    $("#dialog").dialog("open");
                });

                $("#dialog").dialog({
                    autoOpen: false,
                    show: {
                        effect: "blind",
                        duration: 10
                    },
                    hide: {
                        effect: "blind",
                        duration: 10
                    }
                });

                $(".removeItemAction").click(function () {
                    $.ajax({
                        method: "DELETE",
                        url: "<c:url value="/api/items/removeitem/"/>" + $(this).data("itemid")
                    })
                        .done(function () {
                            alert("Item Removed");
                            window.location.reload()
                        })
                });

                $("#saveItem").click(function () {
                    var date = $("#itemForm").serialize();
                    $.ajax({
                        method: "POST",
                        url: "<c:url value="/api/items/saveitem"/>" + "?" + date
                    })
                        .done(function () {
                            alert("Item add");
                            window.location.reload()
                        })
                });

                $("#clearItem").click(function () {
                    $("#itemForm").trigger('reset');
                });

                $(".editItemAction").click(function () {
                    $("#idItem").val($(this).data("itemid"));
                    $("#itemName").val($(this).data("itemname"));
                    $("#itemDescription").val($(this).data("itemdescription"));
                    $("#itemPrice").val($(this).data("itemprise"));
                    $("#dialog").dialog("open");
                });

            });


        </script>

    </tiles:putAttribute>
</tiles:insertDefinition>