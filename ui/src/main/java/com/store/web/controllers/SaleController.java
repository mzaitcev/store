package com.store.web.controllers;

import com.store.core.services.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/sales")
@Controller
public class SaleController {

    @Autowired
    BillService billService;

    @RequestMapping
    public String policyHandler(ModelMap model) {
        model.addAttribute("sales", billService.getAll());
        return "sales/sale";
    }
}
