package com.store.web.controllers;

import com.store.core.services.DiscountServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/discount")
@Controller
public class DiscountController {

    @Autowired
    DiscountServices discountServices;

    @RequestMapping
    public String policyHandler(ModelMap model) {
        model.addAttribute("currDiscount", discountServices.getCurDiscount());
        model.addAttribute("discounts", discountServices.getAllDiscounts());
        return "discount/discount";
    }

}
