package com.store.web.controllers.api;

import com.store.core.domain.StatisticStore;
import com.store.core.services.StatisticService;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/statistic", produces = MediaType.APPLICATION_JSON_VALUE)
public class StatisticApiController {

    @Autowired
    StatisticService statisticService;


    @RequestMapping(value = "/{time}", method = RequestMethod.GET)
    public StatisticStore get(@PathVariable String time) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyyHH:mm:ss");
        DateTime dt = formatter.parseDateTime(time);
        return statisticService.getLastHourStatistic(dt);
    }

}
