package com.store.web.controllers.api;

import com.store.core.domain.Item;
import com.store.core.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/items")
public class ItemApiController {

    @Autowired
    ItemService itemService;

    @RequestMapping(value = "/saveitem", method = RequestMethod.POST)
    public void saveItem(@ModelAttribute Item item) {
        itemService.save(item);
    }

    @RequestMapping(value = "/removeitem/{itemId}", method = RequestMethod.DELETE)
    public void removeItem(@PathVariable long itemId) {
        Item item = itemService.findById(itemId);
        itemService.delete(item);
    }

}
