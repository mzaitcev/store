package com.store.web.controllers;

import com.store.core.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/items")
@Controller
public class ItemController {

    @Autowired
    ItemService itemService;

    @RequestMapping
    public String policyHandler(ModelMap model) {
        model.addAttribute("items", itemService.findAll());
        return "item/item";
    }
}
