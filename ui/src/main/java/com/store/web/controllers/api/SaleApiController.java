package com.store.web.controllers.api;

import com.store.core.domain.Bill;
import com.store.core.domain.Sale;
import com.store.core.services.BillService;
import com.store.core.services.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/sales", produces = MediaType.APPLICATION_JSON_VALUE)
public class SaleApiController {

	@Autowired
	SaleService saleService;
	@Autowired
	BillService billService;

	@RequestMapping(value = "/{billId}", method = RequestMethod.GET)
	public List<Sale> getList(@PathVariable long billId){
		return saleService.getListByBill(billId);
	}

	@RequestMapping(value = "/newSale", method = RequestMethod.POST)
	public void newSale(){
		Bill bill = new Bill();
		billService.saveBill(bill);
	}

	@RequestMapping(value = "/newItem", method = RequestMethod.POST)
	public void newItem(@RequestParam long idSale, @RequestParam long idItem, @RequestParam long itemNumber){
		System.out.println(idSale);
		System.out.println(idItem);
		System.out.println(itemNumber);
		saleService.newSale(idSale, idItem, itemNumber);
	}

}
