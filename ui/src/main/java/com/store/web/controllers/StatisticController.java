package com.store.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/statistic")
@Controller
public class StatisticController {

    @RequestMapping
    public String policyHandler(ModelMap model) {
        return "statistic/statistic";
    }
}
